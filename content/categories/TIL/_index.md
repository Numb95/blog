---
title: "TIL"
description: "Today I learn"
slug: "til"
aliases:
  - til

style:
    background: "#2a9d8f"
    color: "#fff"
    menu: 
        main:
            name: TIL
            weight: -80
            params:
                icon: til

---
