---
title: "TIL: Using git restore to Revert File Changes and Sync with Another Branch"
publishDate: 2024-10-16T13:25:00+02:00
image: "posts/git.png"
published: true
description: "Learn how to use git restore to revert a file's changes and sync it with another branch, such as restoring a file from the main branch to discard local commits."

# _build:
#   render: never
#   list: never
#   # publishResources: false
categories: [
"git",
"TIL"
]
tags: [
"git"
]

---

> **Today I learned** that if you need to revert a specific file to its original
state, even after commits, or if you want to sync a file with another branch,
you can use the `git restore` command.

The `git restore` command allows you to revert a file to its previous state or sync it with a different branch. This can be particularly useful when you need to discard changes and align a file with its version from another branch, such as main. 

In this example, I want to restore and discard all committed changes in my current branch for a specific file to match the version from the main branch:

```bash
git restore --source=main -- main.tf
```

* git-restore [documentation](https://git-scm.com/docs/git-restore)
* Cover image is licensed with [ Creative Commons Attribution 3.0
  Unported](https://creativecommons.org/licenses/by/3.0/deed.en)
  and [source](https://commons.wikimedia.org/wiki/File:Git-logo.svg)
  