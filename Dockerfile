FROM golang:1.16-alpine AS build
#   docker build --build-arg HUGO_BUILD_TAGS=extended .
ARG HUGO_BUILD_TAGS
ARG CGO=1
ENV CGO_ENABLED=${CGO}
ENV GOOS=linux
ENV GO111MODULE=on
WORKDIR /go/src/github.com/gohugoio/hugo
RUN apk update && \
    apk add --no-cache gcc g++ musl-dev git&& \
    go get github.com/magefile/mage
RUN git clone  https://github.com/gohugoio/hugo.git .
RUN mage hugo && mage install
FROM alpine:latest
COPY --from=build /go/bin/hugo /usr/bin/hugo
RUN apk update && \
    apk add --no-cache ca-certificates libc6-compat libstdc++ git
VOLUME /site
WORKDIR /site
EXPOSE 1313
ENTRYPOINT ["hugo"]
CMD ["--help"]
